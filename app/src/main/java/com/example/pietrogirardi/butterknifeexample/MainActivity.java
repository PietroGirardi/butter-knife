package com.example.pietrogirardi.butterknifeexample;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.BindDrawable;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @Bind(R.id.fab) protected  FloatingActionButton fab;
    @Bind(R.id.tvMainTitle) protected TextView tvMainTitle;
    @Bind(R.id.imageView) protected ImageView imageView;
    @Bind(R.id.btCallFragmentTest) protected Button btCallFragmentTest;
    @Bind(R.id.btCallListTest) protected Button btCallListTest;

    @BindDrawable(R.drawable.img_android) protected Drawable drawableImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);

        imageView.setImageDrawable(drawableImage);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.fab)
    public void fabAction(View view){
        Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    @OnClick({R.id.btCallListTest, R.id.btCallFragmentTest})
    public void callOtherScreen(View view){
        Intent intent;

        if(view.getId() == R.id.btCallListTest){
            intent = new Intent(MainActivity.this, ListActivityTest.class);
        }else {
            intent = new Intent(MainActivity.this, SecondActivity.class);
        }
        startActivity(intent);
    }
}
