package com.example.pietrogirardi.butterknifeexample.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.pietrogirardi.butterknifeexample.R;
import com.example.pietrogirardi.butterknifeexample.models.Product;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by pietrogirardi on 28/11/15.
 */
public class ProductAdapter extends BaseAdapter {

    private Context context;
    private List<Product> list;
    private LayoutInflater inflater;


    public ProductAdapter(Context context, List<Product> list){
        this.context = context;
        this.list = list;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if(convertView == null){
            convertView = inflater.inflate(R.layout.item, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);

        }
        else{
            holder = (ViewHolder) convertView.getTag();
        }


        holder.tvName.setText(list.get(position).getName());
        holder.tvCategory.setText(list.get(position).getCategory());
        holder.tvPrice.setText("R$ "+list.get(position).getPriceFormatted());

        return convertView;
    }


    static class ViewHolder{
        @Bind(R.id.tvName) public TextView tvName;
        @Bind(R.id.tvCategory) public TextView tvCategory;
        @Bind(R.id.tvPrice) public TextView tvPrice;

        public ViewHolder(View view){
            ButterKnife.bind(ViewHolder.this, view);
        }
    }

}

